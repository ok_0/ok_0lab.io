# Installation

```bash
git clone https://gitlab.com/ok_0/ok_0.gitlab.com/
cd ok_0.gitlab.com
yarn 
```

# Running

### Generate

```bash
hexo generate
```

### Run server

```bash
hexo server
```
