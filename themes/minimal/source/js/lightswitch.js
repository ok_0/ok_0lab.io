const setLightMode = (isLight, isOnClick) => {
    const darkBtn = document.getElementById("darkBtn");
    const lightBtn = document.getElementById("lightBtn");
    const hljs_theme = document.getElementById("hljs_theme");

    if(isOnClick)
        document.body.classList.toggle('transition');
    
    if(isLight) {
        lightBtn.style.display = "block";
        darkBtn.style.display = "none";
        document.body.classList.replace('dark', 'light');
        hljs_theme.href = "/css/hljs/atom-one-light.css";
        localStorage.setItem("preferredTheme", "light");
    } else {
        lightBtn.style.display = "none";
        darkBtn.style.display = "block";
        document.body.classList.replace('light', 'dark');
        hljs_theme.href = "/css/hljs/atom-one-dark.css";
        localStorage.removeItem("preferredTheme");
    }
    
    if(isOnClick)
        setTimeout(() => document.body.classList.toggle('transition'), 500);
}

if(localStorage.getItem("preferredTheme") == "light") {
    setLightMode(true, false);
} else {
    setLightMode(false, false);
}
