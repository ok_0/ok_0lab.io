const modal = document.querySelector('#share');
const sharelink = document.querySelector('#share-link');    

const toggleModal = () => {
    modal.classList.toggle("hidden");
}

window.onclick = (e) => {
    if(e.target == modal)
        modal.classList.add("hidden"); 
}

const copylink = () => {
    sharelink.select();
    sharelink.setSelectionRange(0, sharelink.value.length);
    navigator.clipboard.writeText(sharelink.value);
}